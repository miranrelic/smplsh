/**
 * @file print_colors.h
 * @author Isus von Bier (isus@podrumibestije.ltd)
 * @brief Color code defines
 * @version 0.1
 * @date 2023-04-30
 *
 *
 */

#ifndef PRINT_COLORS_H
#define PRINT_COLORS_H

#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m"

#endif /* PRINT_COLORS_H */