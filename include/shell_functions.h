/**
 * @file shell_functions.h
 * @author Isus von Bier (isus@podrumibestije.ltd)
 * @brief Function declarations for shell functions
 * @version 0.1
 * @date 2023-04-30
 *
 *
 */

#ifndef SHELL_FUNCTIONS_H
#define SHELL_FUNCTIONS_H

#include "builtin_commands.h"
#include "print_colors.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

/**
  @brief Launch a program and wait for it to terminate.
  @param args Null terminated list of arguments (including program).
  @return Always returns 1, to continue execution.
 */
int smplsh_launch(char** args);

/**
 * @brief Execute shell built-in or launch program.
 * @param args Null terminated list of arguments.
 * @return 0 if it should terminate\n
 *         1 if the shell should continue running\n
 *         2 if error
 *
 */
int smplsh_execute(char** args);

/**
 * @brief Read a line of input from stdin.
 * @return The line from stdin.
 *
 */
char* smplsh_read_line(void);

/**
 * @brief Split a line into tokens (very naively).
 * @param line The line.
 * @return Null-terminated array of tokens.
 *
 */
char** smplsh_split_line(char* line);

/**
 * @brief Loop getting input and executing it.
 *
 */
void smplsh_loop(void);

/**
 * @brief Get number of shell built-in
 *
 * @return Number of shell built-in
 *
 */
int smplsh_num_builtins(void);

/**
 * @brief Print shell prompt with '>' colored (according to status of last
 * command)
 *
 * @param status 1 OK\n
 *               2 NOK
 */
void smplsh_print_prompt(int status);

#endif /* SHELL_FUNCTIONS_H */