/**
 * @file builtin_commands.h
 * @author Isus von Bier (isus@podrumibestije.ltd)
 * @brief Function declarations for builtin commands
 * @version 0.1
 * @date 2023-04-30
 *
 *
 */

#ifndef BUILTIN_COMMANDS_H
#define BUILTIN_COMMANDS_H

#include "shell_functions.h"

#include <time.h>

extern const char* builtin_str[];
extern int (*builtin_func[])(char**);
extern const int builtin_str_size;

/**
 * @brief Builtin command: change directory.
 * @param args List of args.  args[0] is "cd".  args[1] is the directory.
 * @return 1 if the shell should continue running\n
 *         2 if no argument after cd.
 *
 */
int smplsh_cd(char** args);

/**
 * @brief Builtin command: print current directory.
 * @param args List of args.  Not examined.
 * @return Always returns 1, to continue executing.
 */
int smplsh_pwd(char** args);

/**
 * @brief Builtin command: print current user id.
 * @param args List of args.  Not examined.
 * @return Always returns 1, to continue executing.
 */
int smplsh_whoami(char** args);

/**
 * @brief Builtin command: print current user id.
 * @param args List of args.  Not examined.
 * @return Always returns 1, to continue executing.
 */
int smplsh_date(char** args);

/**
 * @brief Builtin command: print current uptime.
 * @param args List of args.  Not examined.
 * @return Always returns 1, to continue executing.
 */
int smplsh_uptime(char** args);

/**
 * @brief Builtin command: reads data from the file.
 * @param args List of args.  args[0] is "cat".  args[1..n] are files to be
 * read.
 * @return 1 if the shell should continue running\n
 *         2 if no argument after cat.
 *
 */
int smplsh_cat(char** args);

/**
 * @brief Builtin command: print help.
 * @param args List of args.  Not examined.
 * @return Always returns 1, to continue executing.
 *
 */
int smplsh_help(char** args);

/**
 * @brief Builtin command: exit.
 * @param args List of args.  Not examined.
 * @return Always returns 0, to terminate execution.
 *
 */
int smplsh_exit(char** args);

#endif /* BUILTIN_COMMANDS_H */