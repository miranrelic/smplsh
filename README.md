# smplsh
[![Build Status](https://lucic.podrumibestije.ltd/buildStatus/icon?job=smplsh)](https://lucic.podrumibestije.ltd/job/smplsh/)

Simple Linux shell written in C

## Usage

- Build and run:
```
$ make
$ ./build/bin/smplsh
```

## Documentation
<sub><sup>* Need to have Doxygen installed</sup></sub>

- Generate documentation:
```
make docs
```

## Tests

- Run tests:
```
make tests
```

## Coverage
<sub><sup>* Need to have gcovr installed</sup></sub>

- Generate coverage:
```
make coverage
```
