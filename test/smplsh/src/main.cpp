#include "CppUTest/CommandLineTestRunner.h"

#include <sys/stat.h>

int main(int ac, char** av)
{
    return CommandLineTestRunner::RunAllTests(ac, av);
}