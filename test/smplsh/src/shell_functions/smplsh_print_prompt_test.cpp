#include "CppUTest/TestHarness.h"

extern "C" {
#include "shell_functions.h"

#include <sys/stat.h>
}

#include <filesystem>
#include <fstream>
#include <sstream>

constexpr char OUTPUT_PATH[] =
    "/build/output/shell_functions/smplsh_print_prompt/";

TEST_GROUP(smplsh_print_prompt_test_group) {
    char m_filepath[PATH_MAX];
    void setup()
    {
        strcpy(m_filepath, std::getenv("PROJ_DIR"));
        strcat(m_filepath, OUTPUT_PATH);
        mkdir(m_filepath, 0700);
        if(!std::filesystem::exists(m_filepath)) {
            std::filesystem::create_directories(m_filepath);
        }
    }
};

TEST(smplsh_print_prompt_test_group, print_prompt) {
    std::ifstream f;
    std::string   file = (std::string)m_filepath + "print_prompt.txt";

    char hostname[1000];
    gethostname(hostname, sizeof(hostname));

    std::string expedtedString;
    std::string stdoutString;
    std::string tempStr;

    std::ostringstream oss;
    oss << BLU << getlogin() << RESET << "@" << GRN << hostname << "\n"
        << GRN << "> " << RESET << "\n";
    expedtedString = oss.str();

    freopen(file.c_str(), "w", stdout);

    smplsh_print_prompt(1);

    freopen("/dev/tty", "w", stdout);

    f.open(file, std::ios::in);

    if(!f.good()) {
        std::string failReason = "Failed to read " + file;
        FAIL(failReason.c_str());
    }
    while(getline(f, tempStr)) {
        stdoutString += tempStr;
        stdoutString += "\n";
    }

    STRCMP_EQUAL(expedtedString.c_str(), stdoutString.c_str());
}