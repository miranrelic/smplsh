#include "CppUTest/TestHarness.h"

extern "C" {
#include "shell_functions.h"
}

TEST_GROUP(smplsh_num_builtins_test_group) {
};

TEST(smplsh_num_builtins_test_group, num_builtins_get) {
    int expected_return = (long unsigned)builtin_str_size / sizeof(char*);

    CHECK_EQUAL(smplsh_num_builtins(), expected_return);
}