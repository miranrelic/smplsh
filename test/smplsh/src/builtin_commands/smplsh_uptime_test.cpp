#include "CppUTest/TestHarness.h"

extern "C" {
#include "builtin_commands.h"

#include <sys/stat.h>
}

#include <filesystem>
#include <fstream>

constexpr char OUTPUT_PATH[] = "/build/output/builtin_commands/smplsh_uptime/";

TEST_GROUP(smplsh_uptime_test_group) {
    char m_filepath[PATH_MAX];
    void setup()
    {
        strcpy(m_filepath, std::getenv("PROJ_DIR"));
        strcat(m_filepath, OUTPUT_PATH);
        mkdir(m_filepath, 0700);
        if(!std::filesystem::exists(m_filepath)) {
            std::filesystem::create_directories(m_filepath);
        }
    }
};

TEST(smplsh_uptime_test_group, uptime_print_output) {
    char*       args[] = {(char*)"uptime"};
    struct stat st;
    long        bytesWritten = 0;
    std::string file = (std::string)m_filepath + "uptime_print_output.txt";

    freopen(file.c_str(), "w", stdout);

    CHECK_EQUAL(smplsh_uptime(args), 1);

    freopen("/dev/tty", "w", stdout);

    stat(file.c_str(), &st);
    bytesWritten = st.st_size;

    CHECK(0 < bytesWritten);
}
