#include "CppUTest/TestHarness.h"

extern "C" {
#include "builtin_commands.h"

#include <sys/stat.h>
}

#include <filesystem>
#include <fstream>

constexpr char OUTPUT_PATH[] = "/build/output/builtin_commands/smplsh_whoami/";

TEST_GROUP(smplsh_whoami_test_group) {
    char m_filepath[PATH_MAX];
    void setup()
    {
        strcpy(m_filepath, std::getenv("PROJ_DIR"));
        strcat(m_filepath, OUTPUT_PATH);
        mkdir(m_filepath, 0700);
        if(!std::filesystem::exists(m_filepath)) {
            std::filesystem::create_directories(m_filepath);
        }
    }
};

TEST(smplsh_whoami_test_group, whoami_print_output) {
    char*       args[] = {(char*)"whoami"};
    struct stat st;
    long        bytesWritten = 0;
    std::string file = (std::string)m_filepath + "whoami_print_output.txt";

    freopen(file.c_str(), "w", stdout);

    CHECK_EQUAL(smplsh_whoami(args), 1);

    freopen("/dev/tty", "w", stdout);

    stat(file.c_str(), &st);
    bytesWritten = st.st_size;

    CHECK(0 < bytesWritten);
}
