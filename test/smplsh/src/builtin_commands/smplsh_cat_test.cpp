#include "CppUTest/TestHarness.h"

extern "C" {
#include "builtin_commands.h"

#include <sys/stat.h>
}

#include <filesystem>
#include <fstream>

constexpr char OUTPUT_PATH[] = "/build/output/builtin_commands/smplsh_cat/";

TEST_GROUP(smplsh_cat_test_group) {
    char m_filepath[PATH_MAX];
    void setup()
    {
        strcpy(m_filepath, std::getenv("PROJ_DIR"));
        strcat(m_filepath, OUTPUT_PATH);
        mkdir(m_filepath, 0700);
        if(!std::filesystem::exists(m_filepath)) {
            std::filesystem::create_directories(m_filepath);
        }
    }
};

TEST(smplsh_cat_test_group, cat_arg1_null) {
    char*         args[] = {(char*)"cat", NULL};
    std::ifstream f;
    std::string   file = (std::string)m_filepath + "cat_arg1_null.txt";

    std::string expedtedString = "smplsh: expected argument/s to \"cat\"";
    std::string stderrString;

    freopen(file.c_str(), "w", stderr);

    CHECK_EQUAL(smplsh_cat(args), 2);

    freopen("/dev/tty", "w", stderr);

    f.open(file, std::ios::in);

    if(!f.good()) {
        std::string failReason = "Failed to read " + file;
        FAIL(failReason.c_str());
    }
    getline(f, stderrString);

    STRCMP_EQUAL(expedtedString.c_str(), stderrString.c_str());
}

TEST(smplsh_cat_test_group, cat_file_not_exist) {
    char*         args[] = {(char*)"cat", (char*)"fake_file"};
    std::ifstream f;
    std::string   file = (std::string)m_filepath + "cat_file_not_exist.txt";

    std::string expedtedString = "smplsh: No such file or directory";
    std::string stderrString;

    freopen(file.c_str(), "w", stderr);

    CHECK_EQUAL(smplsh_cat(args), 2);

    freopen("/dev/tty", "w", stderr);

    f.open(file, std::ios::in);

    if(!f.good()) {
        std::string failReason = "Failed to read " + file;
        FAIL(failReason.c_str());
    }
    getline(f, stderrString);

    STRCMP_EQUAL(expedtedString.c_str(), stderrString.c_str());
}

TEST(smplsh_cat_test_group, cat_print_output) {
    char*       args[] = {(char*)"cat", (char*)"/proc/uptime", NULL};
    struct stat st;
    long        bytesWritten = 0;
    std::string file         = (std::string)m_filepath + "cat_print_output.txt";

    freopen(file.c_str(), "w", stdout);

    CHECK_EQUAL(smplsh_cat(args), 1);

    freopen("/dev/tty", "w", stdout);

    stat(file.c_str(), &st);
    bytesWritten = st.st_size;

    CHECK(0 < bytesWritten);
}