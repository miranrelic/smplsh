#include "CppUTest/TestHarness.h"

extern "C" {
#include "builtin_commands.h"

#include <sys/stat.h>
}

#include <filesystem>
#include <fstream>
#include <sstream>

constexpr char OUTPUT_PATH[] = "/build/output/builtin_commands/smplsh_help/";

TEST_GROUP(smplsh_help_test_group) {
    char m_filepath[PATH_MAX];
    void setup()
    {
        strcpy(m_filepath, std::getenv("PROJ_DIR"));
        strcat(m_filepath, OUTPUT_PATH);
        mkdir(m_filepath, 0700);
        if(!std::filesystem::exists(m_filepath)) {
            std::filesystem::create_directories(m_filepath);
        }
    }
};

TEST(smplsh_help_test_group, help_print) {
    char*         args[] = {(char*)"help"};
    std::ifstream f;
    std::string   file = (std::string)m_filepath + "help_print.txt";

    std::string expedtedString;
    std::string stdoutString;
    std::string tempStr;

    std::ostringstream oss;
    oss << "Isus von Bier's smplsh\n"
        << "Type program names and arguments, and hit enter.\n"
        << "The following are built in:\n";
    for(int i = 0; i < smplsh_num_builtins(); i++) {
        oss << "  " << builtin_str[i] << "\n";
    }
    oss << "Use the man command for information on other programs.\n";

    expedtedString = oss.str();

    freopen(file.c_str(), "w", stdout);

    CHECK_EQUAL(smplsh_help(args), 1);

    freopen("/dev/tty", "w", stdout);

    f.open(file, std::ios::in);

    if(!f.good()) {
        std::string failReason = "Failed to read " + file;
        FAIL(failReason.c_str());
    }
    while(getline(f, tempStr)) {
        stdoutString += tempStr;
        stdoutString += "\n";
    }

    STRCMP_EQUAL(expedtedString.c_str(), stdoutString.c_str());
}