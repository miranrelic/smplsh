#include "CppUTest/TestHarness.h"

extern "C" {
#include "builtin_commands.h"
}

TEST_GROUP(smplsh_exit_test_group) {
};

TEST(smplsh_exit_test_group, exit_test) {
    char* args[] = {(char*)"exit"};
    CHECK_EQUAL(smplsh_exit(args), 0);
}