#include "CppUTest/TestHarness.h"

extern "C" {
#include "builtin_commands.h"

#include <sys/stat.h>
}

#include <filesystem>
#include <fstream>

constexpr char OUTPUT_PATH[] = "/build/output/builtin_commands/smplsh_cd/";

TEST_GROUP(smplsh_cd_test_group) {
    char m_filepath[PATH_MAX];
    void setup()
    {
        strcpy(m_filepath, std::getenv("PROJ_DIR"));
        strcat(m_filepath, OUTPUT_PATH);
        mkdir(m_filepath, 0700);
        if(!std::filesystem::exists(m_filepath)) {
            std::filesystem::create_directories(m_filepath);
        }
    }
};

TEST(smplsh_cd_test_group, cd_arg1_null) {
    char*         args[] = {(char*)"cd", NULL};
    std::ifstream f;
    std::string   file = (std::string)m_filepath + "cd_arg1_null.txt";

    std::string expedtedString = "smplsh: expected argument to \"cd\"";
    std::string stderrString;

    freopen(file.c_str(), "w", stderr);

    CHECK_EQUAL(smplsh_cd(args), 2);

    freopen("/dev/tty", "w", stderr);

    f.open(file, std::ios::in);

    if(!f.good()) {
        std::string failReason = "Failed to read " + file;
        FAIL(failReason.c_str());
    }
    getline(f, stderrString);

    STRCMP_EQUAL(expedtedString.c_str(), stderrString.c_str());
}

TEST(smplsh_cd_test_group, cd_dir_not_exist) {
    char*         args[] = {(char*)"cd", (char*)"fake_dir"};
    std::ifstream f;
    std::string   file = (std::string)m_filepath + "cd_dir_not_exist.txt";

    std::string expedtedString = "smplsh: No such file or directory";
    std::string stderrString;

    freopen(file.c_str(), "w", stderr);

    CHECK_EQUAL(smplsh_cd(args), 2);

    freopen("/dev/tty", "w", stderr);

    f.open(file, std::ios::in);

    if(!f.good()) {
        std::string failReason = "Failed to read " + file;
        FAIL(failReason.c_str());
    }
    getline(f, stderrString);

    STRCMP_EQUAL(expedtedString.c_str(), stderrString.c_str());
}

TEST(smplsh_cd_test_group, cd_dir_success) {
    char*         args[] = {(char*)"cd", (char*)"test_dir"};
    std::ifstream f;
    std::string   file = (std::string)m_filepath + "cd_dir_success.txt";

    char        wd[1000];
    std::string currentDir     = getcwd(wd, sizeof(wd));
    std::string expedtedString = currentDir + "/test_dir";
    std::string stdoutString;

    mkdir("test_dir", 0700);

    freopen(file.c_str(), "w", stdout);

    CHECK_EQUAL(smplsh_cd(args), 1);

    system("pwd");

    freopen("/dev/tty", "w", stdout);

    chdir("..");

    f.open(file, std::ios::in);

    if(!f.good()) {
        std::string failReason = "Failed to read " + file;
        FAIL(failReason.c_str());
    }
    getline(f, stdoutString);
    rmdir("test_dir");

    STRCMP_EQUAL(expedtedString.c_str(), stdoutString.c_str());
}