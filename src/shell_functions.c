/**
 * @file shell_functions.c
 * @author Isus von Bier (isus@podrumibestije.ltd)
 * @brief Function definitions for shell functions
 * @version 0.1
 * @date 2023-04-30
 *
 *
 */

#include "shell_functions.h"

int smplsh_num_builtins(void)
{
    return (long unsigned)builtin_str_size / sizeof(char*);
}

int smplsh_launch(char** args)
{
    pid_t pid;
    int   status;

    pid = fork();
    if(pid == 0) {
        // Child process
        if(execvp(args[0], args) == -1) {
            perror("smplsh");
        }
        exit(EXIT_FAILURE);
    } else if(pid < 0) {
        // Error forking
        perror("smplsh");
        return 2;
    } else {
        // Parent process
        do {
            waitpid(pid, &status, WUNTRACED);
        } while(!WIFEXITED(status) && !WIFSIGNALED(status));
    }

    return 1;
}

int smplsh_execute(char** args)
{
    int i;

    if(args[0] == NULL) {
        // An empty command was entered.
        return 1;
    }

    for(i = 0; i < smplsh_num_builtins(); i++) {
        if(strcmp(args[0], builtin_str[i]) == 0) {
            return (*builtin_func[i])(args);
        }
    }

    return smplsh_launch(args);
}

char* smplsh_read_line(void)
{
    char*  line    = NULL;
    size_t bufsize = 0; // have getline allocate a buffer for us
    if(getline(&line, &bufsize, stdin) == -1) {
        if(feof(stdin)) {
            exit(EXIT_SUCCESS); // We received an EOF
        } else {
            perror("smplsh: getline\n");
            exit(EXIT_FAILURE);
        }
    }
    return line;
}

#define SMPLSH_TOK_BUFSIZE 64
#define SMPLSH_TOK_DELIM   " \t\r\n\a"

char** smplsh_split_line(char* line)
{
    int    bufsize = SMPLSH_TOK_BUFSIZE, position = 0;
    char** tokens = malloc((long unsigned)bufsize * sizeof(char*));
    char * token, **tokens_backup;

    if(!tokens) {
        fprintf(stderr, "smplsh: allocation error\n");
        exit(EXIT_FAILURE);
    }

    token = strtok(line, SMPLSH_TOK_DELIM);
    while(token != NULL) {
        tokens[position] = token;
        position++;

        if(position >= bufsize) {
            bufsize += SMPLSH_TOK_BUFSIZE;
            tokens_backup = tokens;
            tokens = realloc(tokens, (long unsigned)bufsize * sizeof(char*));
            if(!tokens) {
                free(tokens_backup);
                fprintf(stderr, "smplsh: allocation error\n");
                exit(EXIT_FAILURE);
            }
        }

        token = strtok(NULL, SMPLSH_TOK_DELIM);
    }
    tokens[position] = NULL;
    return tokens;
}

void smplsh_print_prompt(int status)
{
    char hostname[1000];
    gethostname(hostname, sizeof(hostname));

    printf(BLU "%s" RESET "@" GRN "%s\n", getlogin(), hostname);
    printf(status == 2 ? RED : GRN);
    printf("> " RESET);
}

void smplsh_loop(void)
{
    char*  line;
    char** args;
    int    status = 1;
    do {
        smplsh_print_prompt(status);
        line   = smplsh_read_line();
        args   = smplsh_split_line(line);
        status = smplsh_execute(args);

        free(line);
        free(args);
    } while(status);
}