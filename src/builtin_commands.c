/**
 * @file builtin_commands.c
 * @author Isus von Bier (isus@podrumibestije.ltd)
 * @brief Function definitions for builtin commands
 * @version 0.1
 * @date 2023-04-30
 *
 *
 */

#include "builtin_commands.h"

const char* builtin_str[] = {
    "cd",
    "pwd",
    "whoami",
    "date",
    "uptime",
    "cat",
    "help",
    "exit",
};

int (*builtin_func[])(char**) = {
    &smplsh_cd,
    &smplsh_pwd,
    &smplsh_whoami,
    &smplsh_date,
    &smplsh_uptime,
    &smplsh_cat,
    &smplsh_help,
    &smplsh_exit,
};

const int builtin_str_size = sizeof(builtin_str);

int smplsh_cd(char** args)
{
    if(args[1] == NULL) {
        fprintf(stderr, "smplsh: expected argument to \"cd\"\n");
        return 2;
    } else {
        if(chdir(args[1]) != 0) {
            perror("smplsh");
            return 2;
        }
    }
    return 1;
}

int smplsh_pwd(char** args)
{
    (void)args;
    char* buf;
    char* ptr;
    long  size = pathconf(".", _PC_PATH_MAX);

    if(size <= 0 || size > 10240) {
        size = 1024;
    }

    if((buf = (char*)malloc((size_t)size)) == NULL) {
        perror("smplsh: failed to allocate memory");
        return 2;
    }
    ptr = getcwd(buf, (size_t)size);
    if(ptr != buf) {
        perror("smplsh: failed to get current working directory");
        return 2;
    }

    printf("Current working directory: %s\n", buf);

    free(buf);
    ptr = NULL;
    buf = NULL;

    return 1;
}

int smplsh_whoami(char** args)
{
    (void)args;
    printf("You are: %s\n", getlogin());
    return 1;
}

int smplsh_date(char** args)
{
    (void)args;
    time_t     t   = time(NULL);
    struct tm* ptm = localtime(&t);
    char       buf[1000];

    strftime(buf, sizeof(buf), "%H:%M:%S %Y-%m-%d %A w%V %Z(%z)", ptm);
    puts(buf);

    return 1;
}

int smplsh_uptime(char** args)
{
    (void)args;
    FILE*      fp;
    char       buf[1000];
    time_t     t   = time(NULL);
    struct tm* ptm = localtime(&t);

    fp = fopen("/proc/uptime", "r");
    if(fp == NULL) {
        fprintf(stderr, "smplsh: Couldn't open \"/proc/uptime\"\n");
        return 2;
    }
    fscanf(fp, "%s", buf);
    fclose(fp);
    long uptime = atol(buf);

    long updays  = uptime / 86400;
    long uphours = (uptime - (updays * 86400)) / 3600;
    long upmins  = (uptime - (updays * 86400) - (uphours * 3600)) / 60;
    long upsecs  = uptime - (updays * 86400) - (uphours * 3600) - (upmins * 60);

    strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", ptm);

    printf("Up for %ld ", updays);
    printf(updays == 1 ? "day" : "days");
    printf(", %ld hours, %ld minutes and %ld seconds at %s\n",
           uphours,
           upmins,
           upsecs,
           buf);

    return 1;
}

int smplsh_cat(char** args)
{
    if(args[1] == NULL) {
        fprintf(stderr, "smplsh: expected argument/s to \"cat\"\n");
        return 2;
    }
    FILE* fp;
    int   ch;

    int i = 1;

    while(args[i] != NULL) {
        fp = fopen(args[i], "r");

        if(NULL == fp) {
            perror("smplsh");
            return 2;
        }

        while((ch = fgetc(fp)) != EOF) {
            printf("%c", ch);
        }

        fclose(fp);
        i++;
    }

    return 1;
}

int smplsh_help(char** args)
{
    (void)args;
    int i;
    printf("Isus von Bier's smplsh\n");
    printf("Type program names and arguments, and hit enter.\n");
    printf("The following are built in:\n");

    for(i = 0; i < smplsh_num_builtins(); i++) {
        printf("  %s\n", builtin_str[i]);
    }

    printf("Use the man command for information on other programs.\n");
    return 1;
}

int smplsh_exit(char** args)
{
    (void)args;
    return 0;
}
