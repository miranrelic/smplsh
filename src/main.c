/**
 * @file main.c
 * @author Isus vshellon Bier (isus@podrumibestije.ltd)
 * @brief A simple Linux shell written in C
 * @version 0.1
 * @date 2023-04-30
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "builtin_commands.h"
#include "shell_functions.h"

/**
 * @brief Main entry point.
 * @param argc Argument count.
 * @param argv Argument vector.
 * @return status code
 */
int main(int argc, char const* argv[])
{
    (void)argc;
    (void)argv;
    // Load config files, if any.

    // Run command loop.
    smplsh_loop();

    // Perform any shutdown/cleanup.

    return EXIT_SUCCESS;
}
