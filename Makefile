CC = gcc

CFLAGS = -Iinclude -g -Wall

INCDIR = ./include
SRCDIR = ./src
TSTDIR = ./test
BLDDIR = ./build
OBJDIR = $(BLDDIR)/obj
BINDIR = $(BLDDIR)/bin
DOCDIR = $(BLDDIR)/docs
COVDIR = $(BLDDIR)/coverage

SRCS = $(wildcard $(SRCDIR)/*.c)
INCS = $(wildcard $(INCDIR)/*.h)
OBJS = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(SRCS))

BIN = $(BINDIR)/smplsh
TEST_BIN_DIR = $(BLDDIR)/UT/bin

export DOC_PATH=$(DOCDIR)
export PROJ_DIR=$(shell pwd)

all: $(BIN)
	@echo "Program compiled"

$(BIN): $(OBJS) | $(BINDIR)
	@echo "Compiling: " $@
	$(CC) $(CFLAGS) $(OBJS) -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)
	@echo "Compiling: " $@
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: docs
docs: $(DOCDIR)/html/index.html
	@echo "Open documentation:"
	@echo
	@echo -e "\t firefox $(DOCDIR)/html/index.html"
	@echo

$(DOCDIR)/html/index.html: ./Doxyfile ./README.md $(SRCS) $(INCS) | $(DOCDIR)
	@doxygen

tests: | $(TEST_BIN_DIR)
	@make -C tools/cpputest
	@make -C $(TSTDIR)

$(TEST_BIN_DIR):
	mkdir -p $(BLDDIR)/UT/bin

.PHONY: coverage
coverage: $(COVDIR)/index.html
	@echo "Open coverage report:"
	@echo
	@echo -e "\t firefox $(COVDIR)/index.html"
	@echo

$(COVDIR)/index.html: $(SRCS) $(INCS) | $(COVDIR)
	@gcovr --html-details -f src/ -e src/main.c -o $(COVDIR)/index.html

$(OBJDIR):
	@mkdir -p $(OBJDIR)

$(BINDIR):
	@mkdir -p $(BINDIR)

$(COVDIR):
	@mkdir -p $(COVDIR)

$(DOCDIR):
	@mkdir -p $(DOCDIR)

clean:
	@rm -rf $(BLDDIR)
	@make -C $(TSTDIR) clean

